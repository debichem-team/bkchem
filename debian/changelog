bkchem (0.14.0~pre4+git20211228-5) unstable; urgency=medium

  * Team upload.
  * Add missing build-dependency on python3-setuptools (Closes: #1080552).
  * Add patch to fix SyntaxWarning from unescaped regular expressions
    (Closes: #1085364).

 -- Stuart Prescott <stuart@debian.org>  Mon, 18 Nov 2024 23:57:44 +1100

bkchem (0.14.0~pre4+git20211228-4) unstable; urgency=medium

  * Team Upload.
  * Switch from imp to importlib for Python 3.12 compatibility, with thanks to
    Vincent Meille for the patch (Closes: #1078148).
  * Update Standards-Version to 4.7.0 (no changes required).

 -- Stuart Prescott <stuart@debian.org>  Thu, 08 Aug 2024 09:34:14 +1000

bkchem (0.14.0~pre4+git20211228-3) unstable; urgency=medium

  * Team Upload.
  * Add patch so that "File properties" no longer raises an exception
    (Closes: #1015822)
  * Change /usr/bin/bkchem to pass through commandline options
    (Closes: #1015821)

 -- Stuart Prescott <stuart@debian.org>  Sat, 12 Nov 2022 00:07:04 +1100

bkchem (0.14.0~pre4+git20211228-2) unstable; urgency=medium

  * Team Upload.
  * Cherry-pick patches from upstream MRs for extra Python 3 fixes:
  * Update Standards-Version to 4.6.1 (no changes required).
  * Add lots of lintian overrides for false positives.
  * Add Rules-Requires-Root: no

 -- Stuart Prescott <stuart@debian.org>  Sun, 06 Nov 2022 23:26:08 +1100

bkchem (0.14.0~pre4+git20211228-1) unstable; urgency=medium

  * Team upload.
  * New upstream git snapshot with Python 3 support.
    - drop all patches, applied upstream.
    - add a new patch to handle config files from old versions.
  * Update upstream VCS location.
  * Change Vcs-* fields to salsa.debian.org.
  * Update build system:
    - use Python 3
    - build with pybuild
    - compile translations at build time
    - build documentation at build time
  * Update to debhelper-compat (= 13).
  * Drop old .menu file.
  * Move private Python module from /usr/lib to /usr/share.
  * Trim trailing whitespace.
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Contact, Name.
  * Update Standards-Version to 4.6.0 (no changes required).

 -- Stuart Prescott <stuart@debian.org>  Sun, 09 Jan 2022 17:00:47 +1100

bkchem (0.13.0-6) unstable; urgency=medium

  * Team upload.
  * Update Standards-Version to 4.1.3 (no changes required).
  * Bump to debhelper compat 11.
  * Update dependencies to python-pil (Closes: #866417).
  * Update Vcs fields to point to new git repo.

 -- Stuart Prescott <stuart@debian.org>  Mon, 08 Jan 2018 16:48:02 +1100

bkchem (0.13.0-5) unstable; urgency=low

  * debian/compat: Bumped to level 9.
  * debian/control (Uploaders): Fixed my address.
    (Build-Depends): Bumped dh version.
    (Build-Depends-Indep): Dropped python-support in favour of dh-python
    (closes: #785963).
    (Standards-Version): Bumped to 3.9.6.
    (DM-Upload-Allowed): Dropped.
    (Vcs-Browser, Vcs-Svn): Fixed vcs-field-not-canonical.
  * debian/rules: Fixed dh syntax for compat level 9.

 -- Daniel Leidert <dleidert@debian.org>  Fri, 21 Aug 2015 15:22:21 +0200

bkchem (0.13.0-4) unstable; urgency=low

  * debian/bkchem.links: Link upstream changelog and fix
    duplicate-changelog-files.
  * debian/compat: Increased dh compat level to 7.
  * debian/control (Standards-Version): Bumped to 3.9.2.
    (Build-Depends): Dropped dpatch. Increased required dh version.
  * debian/rules: Rewritten for dh 7.
  * debian/README.source: Dropped (obsolete).
  * debian/source/format: Added for format 3.0 (quilt).
  * debian/patches/585233_fix_python_26_compatibility.dpatch: Renamed to
    debian/patches/585233_fix_python_26_compatibility.patch.
  * debian/patches/608915_fix_assertion_warnings.dpatch: Renamed to
    debian/patches/608915_fix_assertion_warnings.patch.
  * debian/patches/00list: Renamed to debian/patches/series and adjusted.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sat, 26 Nov 2011 01:04:45 +0100

bkchem (0.13.0-3) unstable; urgency=low

  * debian/control (Standards-Version): Bumped to 3.9.1.
    (Vcs-Browser): Point to real location.
  * debian/copyright: Update. Added missing bits for Pmw.
  * debian/TODO: Updated.
  * debian/patches/585233_fix_python_26_compatibility.dpatch: Adjusted.
    - Fix remaining instances of string exception left (closes: #585233).
  * debian/patches/608915_fix_assertion_warnings.dpatch: Added.
    - Fix syntax warnings about asserts always being true (closes: #608915).
  * debian/patches/00list: Adjusted.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Tue, 08 Feb 2011 02:51:48 +0100

bkchem (0.13.0-2) unstable; urgency=low

  * debian/control (Standards-Version): Bumped to 3.9.0.
    (Build-Depends): Added dpatch. Replaced python-all-dev by python and fix
    build-depends-on-python-dev-with-no-arch-any.
    (Vcs-Svn): Fixed vcs-field-uses-not-recommended-uri-format.
    (Depends): Fixed debhelper-but-no-misc-depends.
    (Description): Removed implementation language.
  * debian/rules: Added dpatch infrastructure.
    (binary-indep): Removed deprecated dh_desktop.
    (.PHONY): Dropped configure target.
  * debian/watch: Slightly improved.
  * debian/README.source: Added because of patch system.
  * debian/patches/585233_fix_python_26_compatibility.dpatch: Added.
    - Fix string exceptions no more allowed in Python 2.6 (closes: #585233).
  * debian/patches/00list: Added accordingly.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Fri, 23 Jul 2010 23:45:25 +0200

bkchem (0.13.0-1) unstable; urgency=low

  * New upstream release 0.13.0.

  * debian/control (Vcs-Svn): Fixed.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Mon, 23 Feb 2009 22:43:18 +0100

bkchem (0.12.6-1) unstable; urgency=low

  * New upstream release 0.12.6.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Wed, 18 Feb 2009 01:13:32 +0100

bkchem (0.12.5-2) unstable; urgency=low

  * Upload to unstable.
  * debian/README.Debian: Fixed two typos. Thanks to lintian.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sun, 15 Feb 2009 19:37:35 +0100

bkchem (0.12.5-1) experimental; urgency=low

  * New upstream release 0.12.5.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Mon, 01 Dec 2008 00:47:01 +0100

bkchem (0.12.4-1) experimental; urgency=low

  * New upstream release 0.12.4.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sun, 02 Nov 2008 11:43:18 +0100

bkchem (0.12.3-1) experimental; urgency=low

  * New upstream release 0.12.3.

  * debian/bkchem.1: Rewritten in pure GROFF.
  * debian/bkchem.1.xml: Therefor removed.
  * debian/bkchem.doc-base (Title, Abstract): Improved wording.
  * debian/bkchem.links: Added tocreate links in the hicolor theme.
  * debian/control (Build-Depends): Removed dpatch, docbook-xsl and xsltproc.
    Split packages into Build-Depends and  Build-Depends-Indep.
    (Standards-Version): Bumped to 3.8.0 (no other changes).
    (Vcs-Svn): Set to experimental location.
  * debian/rules: Removed dpatch stuff. Removed code related to
    debian/bkchem.1 creation.
    (install): Install into /usr/lib/bkchem and strip DESTDIR. So we can
    install without patching upstream build system. Further remove API
    documentation and some files, that were removed by patch in earlier
    package versions.
  * debian/patches/: Removed. We can use the build system without patching.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Mon, 13 Oct 2008 13:19:24 +0200

bkchem (0.12.2-1) unstable; urgency=low

  * New upstream release 0.12.2.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sat, 07 Jun 2008 18:35:55 +0200

bkchem (0.12.1-1) unstable; urgency=low

  * New upstream version 0.12.1.

  * debian/bkchem.1: Added.
  * debian/bkchem.1.xml: Updated.
  * debian/bkchem.doc-base (Section): Fixed accordingly to latest doc-base
    policy version 0.8.10.
  * debian/control: Added DM-Upload-Allowed for DM status.
    (Architecture): This is an architecture-independent package.
    (Uploaders): Removed LI as he retired from the team. Thanks for your work!
  * debian/copyright: Updated.
  * debian/rules: Removed several spaces.
    (build): Added target to create man-page.
    (install): Don't hardcode python version when removing files.
    (binary-indep, binary-arch): This is an architecture-independent package.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sun, 18 May 2008 03:17:10 +0200

bkchem (0.12.0-1) unstable; urgency=low

  * First official build for Debian unstable (closes: #310165).

  * New upstream release 0.12.0.
    - debian/patches/03_oasa_future.dpatch: removed.
    - debian/patches/00list: updated.

 -- LI Daobing <lidaobing@gmail.com>  Sun, 16 Dec 2007 15:04:26 +0800

bkchem (0.11.6-1) unstable; urgency=low

  * New upstream release 0.11.6.

  [ Daniel Leidert ]
  * debian/bkchem.install: Install the official icon to pixmaps path.
  * debian/bkchem.menu: Adjusted icon path.
    (section): Menu section transition.
  * debian/bkchem_icon.xpm: Removed.
  * debian/control: Homepage field transition. Added Vcs-* fields.
    (Maintainer): Set the team as maintainer.
  * debian/copyright: Adjusted copyright. Fixed a typo.
  * debian/watch: Added.

  [ Michael Banck ]
  * debian/bkchem.install: Do not install the removed reportbug override.
  * debian/control (Uploaders): Added myself.
  * debian/README.Debian: Removed unofficial notice.
  * debian/reportbug/control: Removed.

  [ LI Daobing ]
  * debian/control: add me to uploaders.
  * debian/control: bump standards version to 3.7.3.
  * port to python-support.
    - debian/control: update
    - debian/rules: update
    - debian/compat: bump compat level to 5
    - debian/patches/02_fix_build_issues.dpatch: update
  * debian/bkchem.doc-base: remove blank line to satisfy lintian.
  * menu file like xpm file as icon
    - debian/bkchem.xpm: added, convert from bkchem.png and resize to 32x32
    - debian/bkchem.install: install debian/bkchem.xpm
    - debian/bkchem.menu: updated
  * debian/bkchem.dirs: don't install usr/share/bug/bkchem.
  * debian/patches/03_oasa_future.dpatch: python2.5 raise Syntax Error if
    'from __future__' is not in the begin.
  * debian/patches/04_bkchem_bkchem.dpatch: add current dir to sys.path
  * debian/patches/02_fix_build_issues.dpatch:
    + use sys.version instead of hard coded version
    + use `exec' in /usr/bin/bkchem
  * add desktop file support:
    + debian/bkchem.desktop: added
    + debian/bkchem.install: updated
    + debian/rules: add dh_desktop
  * add mime support:
    + debain/bkchem.sharedmimeinfo: added
    + debian/rules: add dh_installmime
    + debian/bkchem.desktop: add mime part

 -- LI Daobing <lidaobing@gmail.com>  Sun, 16 Dec 2007 14:50:27 +0800

bkchem (0.11.4-0dl1) unstable; urgency=low

  * New upstream release 0.11.4.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Mon,  6 Mar 2006 00:05:35 +0100

bkchem (0.11.3-0dl1) unstable; urgency=low

  * New upstream release 0.11.3.
  * debian/README.Debian: Updated.
  * debian/TODO: Updated. Replaces TODO.Debian.
  * debian/rules: Build the manpage during build target.
  * debian/control: Ditto. Added necessary build-dependencies.
  * This package also fixes the problem, that bkchem was installed to
    /usr/share/python-support.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Thu, 16 Feb 2006 00:56:27 +0100

bkchem (0.11.2-0dl1) unstable; urgency=low

  * New upstream release 0.11.2.
  * debian/rules: scratch.py was removed from upstream.
  * debian/README.Debian: python2.3-cairo is part of Debian since a while.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Wed, 11 Jan 2006 21:44:01 +0100

bkchem (0.11.1-0dl1) unstable; urgency=low

  * New upstream release 0.11.1.
  * debian/README.Debian: Removed contrib-comment.
  * debian/rules: Also remove scratch.py from package.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Thu, 29 Dec 2005 13:32:07 +0100

bkchem (0.11.0-0dl1) unstable; urgency=low

  * New upstream release 0.11.0.
  * Moved package to main (no longer needs python2.3-profiler) - thanks to LI
    Daobing for pointing this out.
  * Fixed debian/copyright - updated local license link and FSF address.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Thu, 29 Sep 2005 20:15:45 +0200

bkchem (0.10.2-0dl0) unstable; urgency=low

  * New upstream release 0.10.2.
  * Added suggestion to install python2.3-cairo.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sat, 16 Jul 2005 16:25:00 +0200

bkchem (0.10.1-0dl0) unstable; urgency=low

  * New upstream release 0.10.1.
  * Removed 01_xml_dtd.dpatch - doc.xml is not longer distributed.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Tue,  5 Jul 2005 01:09:29 +0200

bkchem (0.10.0-0dl1) unstable; urgency=low

  * Fixed: 02_fix_build_issues.dpatch - add fix for plugin path.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sun, 26 Jun 2005 02:24:13 +0200

bkchem (0.10.0-0dl0) unstable; urgency=low

  * New upstream release 0.10.0.
  * Fixed: 02_fix_build_issues.dpatch - setup.py has changed and the patch was
    fixed.
  * Fixed: doc-base entry now contains the right section and a better
    abstract.
  * Fixed: copyright should be complete now (missed Piddle notes).
  * Updated: manpage now considers -b option.
  * Updated: standards version now is 3.6.2.1.
  * Added: icon for menu entry.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sun, 26 Jun 2005 00:49:52 +0200

bkchem (0.9.0-0dl0) unstable; urgency=low

  * New upstream release 0.9.0.
  * Package building now uses dpatch.
  * Package moved into contrib.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sat, 30 Apr 2005 17:31:38 +0200

bkchem (0.8.0-0dl0) unstable; urgency=low

  * New upstream release.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Tue, 14 Sep 2004 04:51:48 +0200

bkchem (0.7.1-0dl0) unstable; urgency=low

  * Initial Release.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Thu, 19 Aug 2004 13:02:27 +0200
